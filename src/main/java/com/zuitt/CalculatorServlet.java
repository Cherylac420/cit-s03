package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;

	public void init() throws ServletException {
		System.out.println("****************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("****************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter	out = res.getWriter();
		
		out.println("<h1>You are now using the calculator app</h1>");
		out.println("<div>To use the app, input two numbers and an operation.</div><br>");
		out.println("<div>Hit the submit button after filling in the details.</div><br>");
		out.println("<div>You will get the result shown in your browser.</div>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operator = req.getParameter("operator");
		int answer = 0;
		
		PrintWriter out = res.getWriter();
		
		if(operator.equals("add")) {
			answer = num1 + num2;
		}if(operator.equals("subtract")) {
			answer = num1 - num2;
		}if(operator.equals("multiply")) {
			answer = num1 * num2;
		}if(operator.equals("divide")) {
			answer = num1 / num2;
		}
		
		out.println("<p>The two numbers you provided are: " + num1 + "," + num2 + "</p><br>");
		out.println("<p>The operation that you wanted is: " + operator + "</p><br>");
		out.println("<p>The result is: " + answer + "</p>");
	}
	
	public void destroy(){
		System.out.println("****************************************");
		System.out.println(" CalculatorServlet has been initialized. ");
		System.out.println("****************************************");
	}
}
